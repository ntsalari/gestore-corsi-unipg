from telegram.ext import ConversationHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, error
from datetime import datetime

fmtDataOra = "%d/%m/%Y, %H:%M"

import logging

from QueryController import QueryController

controller = QueryController()

logger = logging.getLogger(__name__)

INIZIO, MESSAGGIO = range(2)

def selezioneUtente(update, context):
    users = controller.getUtentiInChat(update.effective_user.id)
    testoricerca = update.message.text[9:].strip().lower()
    if(len(testoricerca) != 0):
        tmp = list()
        for i in users:
            if i["nome"].lower().find(testoricerca) > -1:
                tmp.append(i)
        if(len(tmp) == 0):
            update.message.reply_text(text="Non ho trovato utenti con il nome corrispondente")
            return ConversationHandler.END
        users = tmp
    if(len(users) == 0):
        update.message.reply_text("Non c'è nessuno...")
        return ConversationHandler.END
    printList = list()
    for i in users:
        tmp = i['nome'] + ": " + str(i['userId'])
        tmp = InlineKeyboardButton(tmp, callback_data='U'+str(i['userId']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Lista di persone connesse al bot\n'
        'Invia /termina per non iniziare la conversazione.', reply_markup = reply_markup)
    return MESSAGGIO

def rispostaUtente(update, context):
    query = update.callback_query
    mieiDati = controller.getUtente(update.effective_user.id)
    context.user_data['mieiDati'] = mieiDati
    conversante = controller.getUtente(query.data[1:])
    for chiave, valore in conversante.items():
        context.user_data[chiave] = valore
    query.answer()
    query.edit_message_text(text="Hai iniziato una conversazione con " + conversante['nome']
    + ", per terminarla usa /termina, se vuoi vedere i vecchi messaggi /messaggi\ne per cancellare tutti i messaggi letti usa /cancellaletti")

def messaggioDocente(update, context):
    docenti = controller.getAllTipo('docente')
    if(len(docenti) == 0):
        return
    printList = list()
    mieiDati = controller.getUtente(update.effective_user.id)
    context.user_data['mieiDati'] = mieiDati
    for i in docenti:
        nome = i['nome']
        tmp = str(nome + ': ' + str(i['userId']))
        tmp = InlineKeyboardButton(tmp, callback_data='L'+str(i['userId']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Scegli il docente', reply_markup = reply_markup)
    return MESSAGGIO

def docenteImpostato(update, context):
    query = update.callback_query
    idDocente = context.user_data['id_docente'] = query.data[1:]
    query.answer()
    corsi = controller.getCorsiProf(idDocente)
    if(len(corsi) == 0):
        query.edit_message_text(text="Il docente selezionato non ha alcun corso assegnato")
        return
    printList = list()
    for i in corsi:
        tmp = i['insegnamento'] + ": " + str(i['id_corso'])
        tmp = InlineKeyboardButton(tmp, callback_data='M'+str(i['id_corso']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    query.edit_message_text('Ora scegli il corso', reply_markup = reply_markup)

def corsoImpostato(update, context):
    query = update.callback_query
    context.user_data['id_corso'] = query.data[1:]
    query.answer()
    query.edit_message_text(text="Ora puoi scrivere il messaggio al docente\nQuando hai fatto usa /termina")

def termina(update, context):

    context.user_data.clear()

    update.message.reply_text(
        'Hai terminato la conversazione!'
    )
    
    return ConversationHandler.END

def cancCronologia(update, context):
    scrittore = context.user_data
    lettore = context.user_data['mieiDati']
    messaggiThread = controller.cancMsgUtente(scrittore['userId'], lettore['userId'])
    update.message.reply_text(text="Ho cancellato la cronologia dei messaggi scritti da: " + scrittore['nome'])
    return MESSAGGIO

def stampaMessaggi(update, context):
    scrittore = context.user_data
    lettore = context.user_data['mieiDati']
    messaggiThread = controller.getMsgPvtUtente(scrittore['userId'], lettore['userId'])
    if len(messaggiThread) > 0:
        update.message.reply_text(text="Messaggi inviati da " + scrittore['nome'])
        for i in messaggiThread:
            update.message.reply_text('Messaggio del: '+i['orario'].strftime(fmtDataOra)+'\n'+i['testo'])
    else:
        update.message.reply_text(text=scrittore['nome'] + " non ti ha ancora inviato nulla")
    return MESSAGGIO

def stampaMessaggiFromStudenti(update, context):
    listaMessaggi = controller.getMessaggiStudenti(update.effective_user.id)
    if len(listaMessaggi) == 0:
        update.message.reply_text(text="Non hai alcun'messaggio")
    for i in listaMessaggi:
        update.message.reply_text(text=i['orario'].strftime(fmtDataOra)+'\n'+i['testo'])

def inviaPriv(update, context):
    utente = context.user_data['mieiDati']
    chat = context.user_data['userId']
    send = "Messaggio inviato da: " + utente['nome'] + '\n' + update.message.text
    try:
        context.bot.send_message(chat_id=chat, text=send)
        controller.addMsgConversazione(utente['userId'], chat, update.message.text)
    except error.TelegramError as e:
        update.message.reply_text(text="Errore invio")
    return MESSAGGIO

def inviaPub(update, context):
    utente = context.user_data['mieiDati']
    id_docente = context.user_data['id_docente']
    corso = controller.getCorso(context.user_data['id_corso'])
    testo = "Messaggio inviato da: " + utente['nome'] + " relativo al corso: " + corso['insegnamento'] + '\n' + update.message.text
    controller.addMsgPerDocente(utente['userId'], id_docente, corso['id_corso'], testo)
    return MESSAGGIO

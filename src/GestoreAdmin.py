from telegram.ext import ConversationHandler, CommandHandler, MessageHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton

import logging

from QueryController import QueryController

logger = logging.getLogger(__name__)
controller = QueryController()

def setGodMode(update, context):
    if controller.isTipo(update.effective_user.id, "amministratore") :
        if(update.message.text.find('on') != -1):
            update.message.reply_text(text="Modalità amministratore attivata /helpadmin")
            context.user_data["admin"] = True
        else:
            update.message.reply_text(text="Modalità amministratore disattivata")
            context.user_data.pop("admin")
    else:
        update.message.reply_text(text="Il comando inserito non è riconosciuto")

def menuUtente(update, context):
    if not "admin" in context.user_data:
        update.message.reply_text(text="Il comando inserito non è riconosciuto")
        return
    users = controller.getUtentiInChat(update.effective_user.id)
    if(len(users) == 0):
        update.message.reply_text("Non c'è nessuno...")
        return ConversationHandler.END
    printList = list()
    for i in users:
        tmp = i['nome'] + ": " + str(i['userId'])
        tmp = InlineKeyboardButton(tmp, callback_data='X'+str(i['userId']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Lista di tutti gli utenti', reply_markup = reply_markup)

def menuTipoAccount(update, context):
    query = update.callback_query
    context.user_data['userId'] = query.data[1:]
    query.answer()
    keyboard = [
        [InlineKeyboardButton("Studente", callback_data="T1")],
        [InlineKeyboardButton("Docente", callback_data="T2")],
        [InlineKeyboardButton("Amministratore", callback_data="T3")]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text("Selezionare il livello di privilegi per l'utente", reply_markup=reply_markup)

def rispostaTipo(update, context):
    query = update.callback_query
    idUtente = context.user_data.pop('userId')
    tipoUtente = int(query.data[1:])
    query.answer()
    if tipoUtente >= 2:
        controller.aggiungiDocente(idUtente)
        if tipoUtente >= 3:
            controller.aggiungiAdmin(idUtente)
    query.edit_message_text("Privilegi aumentati con successo")

def selezioneDocente(update, context):
    if not "admin" in context.user_data:
        update.message.reply_text(text="Il comando inserito non è riconosciuto")
        return
    docenti = controller.getAllTipo('docente')
    if(len(docenti) == 0):
        return
    printList = list()
    for i in docenti:
        nome = i['nome']
        tmp = str(nome + ': ' + str(i['userId']))
        tmp = InlineKeyboardButton(tmp, callback_data='W'+str(i['userId']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Scegli il docente', reply_markup = reply_markup)

def selezioneAssocia(update, context):
    query = update.callback_query
    idDocente = context.user_data['id_docente'] = query.data[1:]
    query.answer()
    corsi = controller.getCorsiNonProf(idDocente)
    if(len(corsi) == 0):
        query.edit_message_text(text="Il docente selezionato è già registrato a tutti i corsi")
        return
    printList = list()
    for i in corsi:
        tmp = i['insegnamento'] + ": " + str(i['id_corso'])
        tmp = InlineKeyboardButton(tmp, callback_data='Z'+str(i['id_corso']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    query.edit_message_text('Ora scegli il corso', reply_markup = reply_markup)

def rispostaDocente(update, context):
    query = update.callback_query
    idCorso = query.data[1:]
    query.answer()
    idDocente = context.user_data.pop("id_docente")
    controller.aggiungiInsegna(idCorso, idDocente)
    query.edit_message_text(text="Associazione fatta :)")

def helpComandi(update, context):
    update.message.reply_text(text="Lista comandi:\n"+
    "/tipoedit Imposta modalità editor (aggiungi/togli) e tipoUtente\n"
    "/editcorsi Aggiunge o toglie associazione docente-corso\n"+
    "/diventaDocente Fa diventare un utente docente")

def esci(update, context):
    update.message.reply_text(text="Azione annullata!")
    return ConversationHandler.END
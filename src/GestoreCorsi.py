from telegram.ext import ConversationHandler, ChatMemberHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, Chat, ChatMember, ChatPermissions, ChatMemberUpdated, error
from typing import Tuple, Optional

import logging

from QueryController import QueryController

controller = QueryController()

logger = logging.getLogger(__name__)

SETUP, NEWPOST = range(2)

def extract_status_change(
    chat_member_update: ChatMemberUpdated,
) -> Optional[Tuple[bool, bool]]:
    """Takes a ChatMemberUpdated instance and extracts whether the 'old_chat_member' was a member
    of the chat and whether the 'new_chat_member' is a member of the chat. Returns None, if
    the status didn't change.
    """
    status_change = chat_member_update.difference().get("status")
    old_is_member, new_is_member = chat_member_update.difference().get("is_member", (None, None))

    if status_change is None:
        return None

    old_status, new_status = status_change
    was_member = (
        old_status
        in [
            ChatMember.MEMBER,
            ChatMember.CREATOR,
            ChatMember.ADMINISTRATOR,
        ]
        or (old_status == ChatMember.RESTRICTED and old_is_member is True)
    )
    is_member = (
        new_status
        in [
            ChatMember.MEMBER,
            ChatMember.CREATOR,
            ChatMember.ADMINISTRATOR,
        ]
        or (new_status == ChatMember.RESTRICTED and new_is_member is True)
    )

    return was_member, is_member

def setupChat(update, context):
    chat = update.effective_chat
    if chat.type in [Chat.GROUP, Chat.SUPERGROUP]:
        try:
            permessi = ChatPermissions(can_send_messages=False, can_send_media_messages=False, can_send_polls=False, can_send_other_messages=False,
            can_add_web_page_previews=False, can_change_info=False, can_invite_users=False, can_pin_messages=False)
            context.bot.set_chat_permissions(chat.id, permessi)
            link = context.bot.create_chat_invite_link(chat.id)
            controller.reSetCorso([chat.id, chat.title, link.invite_link])
        except error.TelegramError as ex:
            '''try:
                context.bot.send_message(chat_id=chat.id, text="Prima di /setup devi nominarmi amministratore")
            except error.TelegramError as ex:'''
            userId = update.effective_user.id
            context.bot.send_message(chat_id=userId, text="Messaggio relativo alla chat: "+ chat.title+
            "\nPrima di dare il comando /setup devi nominarmi amministratore")

def aggiungiChat(update, context):
    result = extract_status_change(update.my_chat_member)
    if result == None:
        return
    wasMember, isMember = result
    chat = update.effective_chat
    if chat.type == Chat.PRIVATE:
        return
    elif chat.type in [Chat.GROUP, Chat.SUPERGROUP]:
        if not wasMember and isMember:
            try:
                context.bot.send_message(chat_id=chat.id, text="Per favore nominami amministratore e dai il comando /setup")
            except error.TelegramError as e:
                pass
            controller.aggiungiCorso([chat.id, chat.title, "NULL"])
        controller.setNonDisponibile(chat.id)
    else:
        if not wasMember and isMember:
            link = context.bot.create_chat_invite_link(chat.id)
            test = controller.getCorsoByChat(chat.id)
            if(test == None):
                controller.aggiungiCorso([chat.id, chat.title, link.invite_link])
            else:
                controller.reSetCorso([chat.id, chat.title, link.invite_link])
        elif wasMember and not isMember:
            controller.setNonDisponibile(chat.id)
        
def rispostaCorso(update, context):
    query = update.callback_query
    corso = controller.getCorso(query.data[1:])
    for chiave, valore in corso.items():
        context.user_data[chiave] = valore
    query.answer()
    query.edit_message_text(text="Hai selezionato il corso, puoi inviare tutti i messaggi che ti servono\n"+
    "quando hai fatto usa /termina")

def posta(update, context):
    chat = context.user_data['id_chat']
    try:
        context.bot.copy_message(chat_id=chat, from_chat_id=update.effective_chat.id, message_id=update.message.message_id)
        controller.addMsgCorso(update.effective_user.id, context.user_data['id_corso'], update.message.text)
    except error.TelegramError as e:
        update.message.reply_text(text="Errore invio")
    return NEWPOST

def selezioneCorso(update, context):
    corsi = None
    if controller.isTipo(update.effective_user.id, "docente") :
        corsi = controller.getCorsiProf(update.effective_user.id)
    else:
        update.message.reply_text("Questa funzione riservata ai docenti")
        return ConversationHandler.END
    if(len(corsi) == 0):
        update.message.reply_text("Non insegni alcun'corso, contatta l'amministratore")
        return ConversationHandler.END
    printList = list()
    for i in corsi:
        tmp = i['insegnamento'] + ": " + str(i['id_corso'])
        tmp = InlineKeyboardButton(tmp, callback_data='C'+str(i['id_corso']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Corsi di cui sei il docente\nSe hai cambiato idea usa /termina', reply_markup = reply_markup)
    return NEWPOST

def studentiSegue(update, context):
    corsi = None
    if controller.isTipo(update.effective_user.id, "docente") :
        corsi = controller.getCorsiProf(update.effective_user.id)
    else:
        update.message.reply_text("Questa funzione è riservata ai docenti")
        return
    if(len(corsi) == 0):
        update.message.reply_text("Non insegni alcun'corso, contatta l'amministratore")
        return
    printList = list()
    for i in corsi:
        tmp = i['insegnamento'] + ": " + str(i['id_corso'])
        tmp = InlineKeyboardButton(tmp, callback_data='N'+str(i['id_corso']))
        printList.append( [tmp] )
    reply_markup = InlineKeyboardMarkup(printList)
    update.message.reply_text('Corsi di cui sei il docente', reply_markup = reply_markup)

def stampaListaStudenti(update, context):
    query = update.callback_query
    corso = controller.getCorso(query.data[1:])
    studenti = controller.getIscritti(corso['id_corso'])
    query.answer()
    testo = "Lista iscritti: \n"
    for i in studenti:
        testo = testo + i["nome"]
        if i["cognome"] is not None:
            testo = testo + ' ' + i["cognome"]
        testo = testo + '\n'
    query.edit_message_text(text=testo)

def stop(update, context):
    context.user_data.clear()

    update.message.reply_text(
        'Ok!'
    )

    return ConversationHandler.END

'''def controlloIscritti(update, context):
    result = extract_status_change(update.chat_member)
    logger.info("Qualcosa è successo!")
    if result is None:
        return
    corso = controller.getCorsoByChat(update.effective_chat.id)
    if (corso == None) or (corso["disponibile"] == 0):
        return
    
    wasMember, isMember = result
    
    if not wasMember and isMember:
        utente = update.chat_member.new_chat_member.user
        check = controller.getUtente(utente.id)
        context.bot.ban_chat_member(update.effective_chat.id, utente.id)
        if(check != None):
            context.bot.send_message(chat_id=check['userId'], text="Devi prima iscriverti dal menu che trovi su /segue")
    elif wasMember and not isMember:
        idDisiscritto = update.chat_member.from_user.id
        controller.rimuoviStudente(idDisiscritto, corso["id_corso"])'''
        
import pymysql.cursors
import logging

from Singleton import Singleton

logger = logging.getLogger(__name__)

class QueryController(metaclass=Singleton):
    def __init__(self):
        pass
    def __connetti__(self):
        conn = None
        try:
            conn = pymysql.connect(
                host = 'localhost',
                user = 'admin',
                password = 'GreenYoshi.',
                database = 'unipg',
                charset = 'utf8mb4',
                cursorclass=pymysql.cursors.DictCursor
            )
        except:
            pass
        return conn
    def getUtentiInChat(self, u):
        listaOn = list()
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `utente` WHERE `userId` != %s "
            cursor.execute(sql, (int(u)))
            listaOn = list(cursor.fetchall())
        conn.close()
        return listaOn
    def getAllTipo(self, t):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `utente`"+
            " INNER JOIN `"+t+"` ON `"+t+"`.`userId` = `utente`.`userId`")
            cursor.execute(sql)
            listaTipo = list(cursor.fetchall())
        conn.close()
        return listaTipo
    def getUtente(self, u):
        utente = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `utente` WHERE `userId` = %s "
            cursor.execute(sql, (int(u)))
            utente = cursor.fetchone()
        conn.close()
        return utente
    def getMessaggiStudenti(self, c):
        conn = self.__connetti__()
        listaMsg = list()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `messaggio`"+
                    "WHERE `id_lettore` = %s AND `id_corso` IS NOT NULL")
            cursor.execute(sql, (c))
            listaMsg = list(cursor.fetchall())
        with conn.cursor() as cursor:
            sql = ("DELETE FROM `messaggio`"+
                    "WHERE `id_lettore` = %s AND `id_corso` IS NOT NULL")
            cursor.execute(sql, (c))
        conn.commit()
        conn.close()
        return listaMsg
    def getByNome(self, nome):
        lista = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `utente` WHERE `nome` = %s"
            cursor.execute(sql, (nome))
            lista = list(cursor.fetchall())
        return lista
        conn.close()
    def __aggiungiUtente__(self, u):
        if(isinstance(u, list)):
            conn = self.__connetti__()
            with conn.cursor() as cursor:
                sql = "INSERT INTO `utente` (`userId`, `nome`, `cognome`, `username`, `id_chat`) VALUES (%s, %s, %s, %s, %s)"
                cursor.execute(sql, (int(u[0]), u[1], u[2], u[3], int(u[4])))
            conn.commit()
            conn.close()
    def aggiungiDocente(self, d):
        if(isinstance(d, list)):
            conn = self.__connetti__()
            self.__aggiungiUtente__(d)
            with conn.cursor() as cursor:
                sql = "INSERT INTO `docente` (`userId`) VALUES (%s)"
                cursor.execute(sql, (int(d[0])))
            conn.commit()
            conn.close()
    def aggiungiStudente(self, s):
        if(isinstance(s, list)):
            conn = self.__connetti__()
            self.__aggiungiUtente__(s)
            with conn.cursor() as cursor:
                sql = "INSERT INTO `studente` (`userId`) VALUES (%s)"
                cursor.execute(sql, (int(s[0])))
            conn.commit()
            conn.close()
    def aggiungiInsegna(self, c, d):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "INSERT IGNORE INTO `insegna` (`id_docente`, `id_corso`) VALUES (%s, %s)"
            cursor.execute(sql, (int(d), int(c)))
        conn.commit()
        conn.close()
    def aggiungiDocente(self, u):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "INSERT IGNORE INTO `docente` (`userId`) VALUES (%s)"
            cursor.execute(sql, (int(u)))
        conn.commit()
        conn.close()
    def aggiungiAdmin(self, u):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "INSERT IGNORE INTO `amministratore` (`userId`) VALUES (%s)"
            cursor.execute(sql, (int(u)))
        conn.commit()
        conn.close()
    def setChat(self, u, c):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "UPDATE `utente` SET `id_chat` = %s WHERE `userId` = %s"
            cursor.execute(sql, (int(c), int(u)))
        conn.commit()
        conn.close()
    def isTipo(self, u, tipo):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `" + tipo + "` WHERE `userId` = %s"
            cursor.execute(sql, (int(u)))
            t = cursor.fetchone()
            if(t == None):
                return False
            else:
                return True
        conn.close()
    def getCorsiProf(self, p):
        corsi = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `corso`"+
                " INNER JOIN `insegna` ON `corso`.`id_corso` = `insegna`.`id_corso`"+
                " WHERE `id_docente` = %s AND `disponibile` = 1")
            cursor.execute(sql, (int(p)))
            corsi = list(cursor.fetchall())
        conn.close()
        return corsi
    def getCorsiNonProf(self, p):
        corsi = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `corso`"+
                " WHERE `disponibile` = 1 EXCEPT"+
                " SELECT `corso`.`id_corso`, `id_chat`, `insegnamento`, `disponibile`, `linkInvito` FROM `corso`"+
                " INNER JOIN `insegna` ON `corso`.`id_corso` = `insegna`.`id_corso`"+
                " WHERE `id_docente` = %s")
            cursor.execute(sql, (int(p)))
            corsi = list(cursor.fetchall())
        conn.close()
        return corsi
    def getCorso(self, c):
        corso = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `corso` WHERE `id_corso` = %s"
            cursor.execute(sql, (int(c)))
            corso = cursor.fetchone()
        conn.close()
        return corso
    def getCorsoByChat(self, c):
        corso = None
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "SELECT * FROM `corso` WHERE `id_chat` = %s"
            cursor.execute(sql, (int(c)))
            corso = cursor.fetchone()
        conn.close()
        return corso
    def aggiungiCorso(self, c):
        if isinstance(c, list):
            conn = self.__connetti__()
            with conn.cursor() as cursor:
                sql = "INSERT INTO `corso` (`id_chat`, `insegnamento`, `disponibile`, `linkInvito`) VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (int(c[0]), c[1], 1, c[2]))
            conn.commit()
            conn.close()
    def reSetCorso(self, c):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "UPDATE `corso` SET `insegnamento` = %s, `linkInvito` = %s, `disponibile` = 1 WHERE `id_chat` = %s"
            cursor.execute(sql, (c[1], c[2], int(c[0])))
        conn.commit()
        conn.close()
    def setNonDisponibile(self, id):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "UPDATE `corso` SET `disponibile` = 0, `linkInvito` = NULL WHERE `id_chat` = %s"
            cursor.execute(sql, (int(id)))
        conn.commit()
        conn.close()
    def getCorsiDisponibili(self, u):
        listaC = list()
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `corso`"+
            " WHERE `disponibile` = 1 EXCEPT"+
            " SELECT DISTINCT `corso`.`id_corso`, `id_chat`, `insegnamento`, `disponibile`, `linkInvito` FROM `corso`"+
            " INNER JOIN `segue` ON `corso`.`id_corso` = `segue`.`id_corso`"+
            " WHERE `id_studente` = %s")
            cursor.execute(sql, (int(u)))
            listaC = list(cursor.fetchall())
        conn.close()
        return listaC
    def getCorsiStudente(self, u):
        listaC = list()
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT DISTINCT `corso`.`id_corso`, `id_chat`, `insegnamento`, `disponibile`, `linkInvito` FROM `corso`"+
            " INNER JOIN `segue` ON `corso`.`id_corso` = `segue`.`id_corso`"+
            " WHERE `id_studente` = %s AND `disponibile` = 1")
            cursor.execute(sql, (int(u)))
            listaC = list(cursor.fetchall())
        conn.close()
        return listaC
    def seguiCorso(self, s, c):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "INSERT INTO `segue` (`id_studente`, `id_corso`) VALUES (%s, %s)"
            cursor.execute(sql, (int(s), int(c)))
        conn.commit()
        conn.close()
    def cancellaSeguiCorso(self, s, c):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = "DELETE FROM `segue` WHERE `id_studente` = %s AND `id_corso` = %s"
            cursor.execute(sql, (int(s), int(c)))
        conn.commit()
        conn.close()
    def addMsgConversazione(self, id_scrittore, id_lettore, testo):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("INSERT INTO `messaggio` (`id_scrittore`, `id_lettore`, `testo`)"+
                    " VALUES(%s, %s, %s)")
            cursor.execute(sql, (id_scrittore, id_lettore, testo))
        conn.commit()
        conn.close()
    def addMsgCorso(self, id_scrittore, id_corso, testo):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("INSERT INTO `messaggio` (`id_scrittore`, `id_corso`, `testo`)"+
                    " VALUES(%s, %s, %s)")
            cursor.execute(sql, (id_scrittore, id_corso, testo))
        conn.commit()
        conn.close()
    def addMsgPerDocente(self, id_scrittore, id_docente, id_corso, testo):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("INSERT INTO `messaggio` (`id_scrittore`, `id_lettore`, `id_corso`, `testo`)"+
                    " VALUES(%s, %s, %s, %s)")
            cursor.execute(sql, (id_scrittore, id_docente, id_corso, testo))
        conn.commit()
        conn.close()
    def getMsgCorso(self, id_corso):
        listaMsg = list()
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `messaggio`"+
                    "WHERE `id_corso` = %s AND `id_lettore` IS NULL")
            cursor.execute(sql, (id_corso))
            listaMsg = list(cursor.fetchall())
        conn.close()
        return listaMsg
    def getMsgPvtUtente(self, id_scrittore, id_lettore):
        conn = self.__connetti__()
        listaMsg = list()
        with conn.cursor() as cursor:
            sql = ("SELECT * FROM `messaggio`"+
                    " WHERE `id_scrittore` = %s AND `id_lettore` = %s AND `id_corso` IS NULL")
            cursor.execute(sql, (id_scrittore, id_lettore))
            listaMsg = list(cursor.fetchall())
        conn.close()
        return listaMsg
    def cancMsgUtente(self, id_scrittore, id_lettore):
        conn = self.__connetti__()
        with conn.cursor() as cursor:
            sql = ("DELETE FROM `messaggio`"+
                    " WHERE `id_scrittore` = %s AND `id_lettore` = %s AND `id_corso` IS NULL")
            cursor.execute(sql, (id_scrittore, id_lettore))
        conn.commit()
        conn.close()
    def getIscritti(self, c):
        conn = conn = self.__connetti__()
        studenti = list()
        with conn.cursor() as cursor:
            sql = ("SELECT `nome`, `cognome` FROM `utente`"+
                    " INNER JOIN `studente` ON `studente`.`userId` = `utente`.`userId`"+
                    " INNER JOIN `segue` ON `id_studente` = `utente`.`userId`"+
                    " WHERE `id_corso` = %s"
                )
            cursor.execute(sql, (c))
            studenti = list(cursor.fetchall())
        conn.close()
        return studenti

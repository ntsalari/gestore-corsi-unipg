from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import Chat

import logging

import sys
sys.path.append(".")

from QueryController import QueryController
from GestoreConversazioni import *
from GestoreCorsi import *
from GestoreAdmin import *
from GestoreIscrizioni import *

controller = QueryController()

logger = logging.getLogger(__name__)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def start(update, context):
    user = update.effective_user
    update.message.reply_text(text="Benvenuto al bot di gestione corsi!\nTi consiglio di dare il comando /help")
    collisione = controller.getUtente(user.id)
    if collisione != None:
        controller.setChat(user.id, update.effective_chat.id)
    else:
        controller.aggiungiStudente([user.id, user.first_name, user.last_name, user.username, update.effective_chat.id])

def comandoS(update, context):
    update.message.reply_text(text="Il comando inserito non è riconosciuto")

def noChat(update, context):
    if update.effective_chat.type in Chat.PRIVATE:
        update.message.reply_text(text="Per favore usa uno dei comandi descritti da /help")

def soloTesto(update, context):
    update.message.reply_text(text="Mi dispiace ma nelle conversazioni private si possono mandare solo messaggi testuali")

def nonSupportato(update, context):
    update.message.reply_text(text="Il tipo di documento che stai inviando non è supportato")

def noNidificati(update, context):
    update.message.reply_text(text="Non è possibile nidificare comandi")

def helpTermina(update, context):
    update.message.reply_text(text="Puoi usare termina solo per /posta e /conversa")

def help(update, context):
    update.message.reply_text(text="Comandi:\n/start Registrati e avvia il bot\n/conversa Scegli utente con cui vuoi iniziare una conversazione\n"+
    "/termina Termina il comando corrente\n/posta Posta un messaggio in un corso (docenti)\n"+
    "/segui Segui un corso tra i disponibili (studenti)\n/cancella Cancellati da un corso (studenti)\n"+
    "/cronologia Stampa cronologia post di un corso (studenti)\n/messaggi Stampa messaggi vecchi (dell'utente scelto con /conversa)\n/help Mostra questo messaggio")

def helpadmin(update, context):
    if "admin" in context.user_data:
        update.message.reply_text(text="Comandi:\n/privilegi Aumentare privilegi ad un utente\n/aggiungi Crea associazione docente-corso")
    else:
        comandoS(update, context)

def main():
    token = open("../token.txt").read()

    updater = Updater(token=token, use_context = True) #Connessione all'API di telegram

    dispatcher = updater.dispatcher

    no_nidificati = MessageHandler(Filters.command, noNidificati)
    
    corsi_handler = ChatMemberHandler(aggiungiChat, ChatMemberHandler.MY_CHAT_MEMBER)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('conversa', selezioneUtente)],
        states={
            MESSAGGIO: [CommandHandler('cancellaletti', cancCronologia),
                        CommandHandler('messaggi', stampaMessaggi),
                        MessageHandler(Filters.text & ~Filters.command, inviaPriv),
                        ],
        },
        fallbacks=[CommandHandler('termina', termina),
         no_nidificati, MessageHandler(Filters.all & ~Filters.text, soloTesto)
        ],
    )

    post_handler = ConversationHandler(
        entry_points=[CommandHandler('posta', selezioneCorso)],
        states={
            NEWPOST: [MessageHandler(Filters.all & ~Filters.command, posta)],
        },
        fallbacks=[CommandHandler('termina', stop), no_nidificati
        ],
    )
    
    req_handler = ConversationHandler(
        entry_points=[CommandHandler('richiesta', messaggioDocente)],
        states={
            MESSAGGIO: [
                        MessageHandler(Filters.text & ~Filters.command, inviaPub),
                        ],
        },
        fallbacks=[CommandHandler('termina', termina),
         no_nidificati, MessageHandler(Filters.all & ~Filters.text, soloTesto)
        ],
    )

    dispatcher.add_handler(corsi_handler)
    
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(post_handler)
    dispatcher.add_handler(req_handler)
    dispatcher.add_handler(CommandHandler('segui', iscriviti))
    dispatcher.add_handler(CommandHandler('cancella', cancellaIscrizione))
    dispatcher.add_handler(CommandHandler('setup', setupChat))
    dispatcher.add_handler(CommandHandler('godmode', setGodMode))
    dispatcher.add_handler(CommandHandler('cronologia', elencoPost))
    dispatcher.add_handler(CommandHandler('termina', helpTermina))
    dispatcher.add_handler(CommandHandler('aggiungi', selezioneDocente))
    dispatcher.add_handler(CommandHandler('privilegi', menuUtente))
    dispatcher.add_handler(CommandHandler('helpadmin', helpadmin))
    dispatcher.add_handler(CommandHandler('messaggistudenti', stampaMessaggiFromStudenti))
    dispatcher.add_handler(CommandHandler('listastudenti', studentiSegue))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaUtente, pattern="U\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaCorso, pattern="C\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaIscrizione, pattern="I\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaCancella, pattern="D\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaPost, pattern="H\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = selezioneAssocia, pattern="W\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaDocente, pattern="Z\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = menuTipoAccount, pattern="X\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = rispostaTipo, pattern="T\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = docenteImpostato, pattern="L\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = corsoImpostato, pattern="M\d*"))
    dispatcher.add_handler(CallbackQueryHandler(callback = stampaListaStudenti, pattern="N\d*"))
    

    
    dispatcher.add_handler(MessageHandler(Filters.command, comandoS))
    dispatcher.add_handler(MessageHandler(Filters.all, noChat))

    updater.start_polling()


if __name__ == '__main__':
    main()
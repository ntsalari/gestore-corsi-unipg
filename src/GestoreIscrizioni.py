from telegram.ext import ConversationHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram import error
from datetime import datetime

import logging

from QueryController import QueryController

controller = QueryController()

logger = logging.getLogger(__name__)

fmtDataOra = "%d/%m/%Y, %H:%M"

def iscriviti(update, context):
    if( not controller.isTipo(update.effective_user.id, "studente")):
        update.message.reply_text(text="Questa funzione è riservata agli studenti")
        return
    corsi = controller.getCorsiDisponibili(update.effective_user.id)
    testoricerca = update.message.text[6:].strip().lower()
    if(len(testoricerca) != 0):
        tmp = list()
        for i in corsi:
            if i["insegnamento"].lower().find(testoricerca) > -1:
                tmp.append(i)
        if(len(tmp) == 0):
            update.message.reply_text(text="Non ho trovato corsi corrispondenti a cui iscriverti")
            return
        corsi = tmp
    if(len(corsi) == 0):
        update.message.reply_text(text="Ti sei già iscritto a tutti i corsi")
        return
    printList = list()
    for i in corsi:
        tmp = i["insegnamento"]
        tmp = InlineKeyboardButton(tmp, callback_data="I"+str(i["id_corso"]))
        printList.append( [tmp] )
    reply_markup= InlineKeyboardMarkup(printList)
    update.message.reply_text(text="Ecco a te la lista dei corsi, scegli pure", reply_markup=reply_markup)

def rispostaIscrizione(update, context):
    query = update.callback_query
    idCorso = query.data[1:]
    query.answer()
    controller.seguiCorso(update.effective_user.id, idCorso)
    corso = controller.getCorso(idCorso)
    try:
        context.bot.unban_chat_member(corso['id_chat'], update.effective_user.id)
    except error.TelegramError as ex:
        pass
    query.edit_message_text(text="Ti ho iscritto al corso, ora ecco il link:\n" + corso['linkInvito'])

def cancellaIscrizione(update, context):
    if( not controller.isTipo(update.effective_user.id, "studente")):
        update.message.reply_text(text="Questa funzione è riservata agli studenti")
        return
    corsi = controller.getCorsiStudente(update.effective_user.id)
    if(len(corsi) == 0):
        update.message.reply_text(text="Non sei iscritto a nessun corso")
        return
    printList = list()
    for i in corsi:
        tmp = i["insegnamento"]
        tmp = InlineKeyboardButton(tmp, callback_data="D"+str(i["id_corso"]))
        printList.append( [tmp] )
    reply_markup= InlineKeyboardMarkup(printList)
    update.message.reply_text(text="Ecco a te la lista dei corsi, scegli pure\nSe hai cambiato idea usa /termina", reply_markup=reply_markup)

def rispostaCancella(update, context):
    query = update.callback_query
    idCorso = query.data[1:]
    query.answer()
    controller.cancellaSeguiCorso(update.effective_user.id, idCorso)
    idChat = controller.getCorso(idCorso)["id_chat"]
    try:
        context.bot.ban_chat_member(idChat, update.effective_user.id)
    except error.TelegramError as ex:
        pass
    query.edit_message_text(text="Ti ho rimosso con successo")

def elencoPost(update, context):
    if( not controller.isTipo(update.effective_user.id, "studente")):
        update.message.reply_text(text="Questa funzione è riservata agli studenti")
        return
    corsi = controller.getCorsiStudente(update.effective_user.id)
    if(len(corsi) == 0):
        update.message.reply_text(text="Non sei iscritto a nessun corso")
        return
    printList = list()
    for i in corsi:
        tmp = i["insegnamento"]
        tmp = InlineKeyboardButton(tmp, callback_data="H"+str(i["id_corso"]))
        printList.append( [tmp] )
    reply_markup= InlineKeyboardMarkup(printList)
    update.message.reply_text(text="Ecco a te la lista dei corsi, scegli pure\nSe hai cambiato idea usa /termina", reply_markup=reply_markup)

def rispostaPost(update, context):
    query = update.callback_query
    idCorso = query.data[1:]
    query.answer()
    messaggiCorso = controller.getMsgCorso(idCorso)
    if len(messaggiCorso) > 0:
        query.edit_message_text(text="Messaggi del corso:")
        for i in messaggiCorso:
            try:
                context.bot.send_message(chat_id=update.effective_user.id, text="Messaggio del: " + i['orario'].strftime(fmtDataOra)+ '\n' +i["testo"])
            except error.TelegramError as ex:
                context.bot.send_message(chat_id=update.effective_user.id, text="Messaggio del: " + i['orario'].strftime(fmtDataOra)+ '\n' + "Contenuto multimediale")
    else:
        query.edit_message_text(text="Il corso selezionato non ha ancora alcun messaggio")


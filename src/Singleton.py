#Implementazione elegante del pattern Singleton sulla classe Controller
#permessa da Python 3 grazie all'uso di metaclass (classe di classi)
class Singleton(type):
    _istanze = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._istanze:
            cls._istanze[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        #else:
        #    cls._istanze[cls].__init__(*args, **kwargs)
        return cls._istanze[cls]
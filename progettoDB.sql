CREATE DATABASE unipg;
USE unipg;
CREATE TABLE utente (
    userId BIGINT PRIMARY KEY,
    nome TEXT NOT NULL,
    cognome TEXT,
    username TEXT,
    id_chat BIGINT UNIQUE NOT NULL
);
CREATE TABLE docente(
    userId BIGINT PRIMARY KEY,
    FOREIGN KEY(userId) REFERENCES utente(userId)
);
CREATE TABLE studente(
    userId BIGINT PRIMARY KEY,
    FOREIGN KEY(userId) REFERENCES utente(userId)
);
CREATE TABLE amministratore(
    userId BIGINT PRIMARY KEY, 
    FOREIGN KEY(userId) REFERENCES utente(userId)
);
CREATE TABLE corso (
    id_corso BIGINT AUTO_INCREMENT PRIMARY KEY,
    id_chat BIGINT NOT NULL,
    insegnamento TEXT NOT NULL,
    disponibile BIT NOT NULL,
    linkInvito TEXT
);
/*CREATE TABLE prenotazione(
    id_prenotazione BIGINT AUTO_INCREMENT PRIMARY KEY,
    orario TIME NOT NULL,
    data DATE NOT NULL,
    id_docente BIGINT NOT NULL,
    id_studente BIGINT NOT NULL,
    UNIQUE(orario, data, id_docente),
    FOREIGN KEY (id_docente) REFERENCES docente(userId),
    FOREIGN KEY (id_studente) REFERENCES studente(userId)
);*/
CREATE TABLE messaggio(
    id_messaggio BIGINT AUTO_INCREMENT PRIMARY KEY,
    testo TEXT,
    orario TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    id_corso BIGINT,
    id_scrittore BIGINT NOT NULL,
    id_lettore BIGINT,
    CHECK(id_scrittore != id_lettore),
    CHECK(id_lettore IS NOT NULL || id_corso IS NOT NULL),
    FOREIGN KEY (id_scrittore) REFERENCES utente(userId),
    FOREIGN KEY (id_lettore) REFERENCES utente(userId),
    FOREIGN KEY (id_corso) REFERENCES corso(id_corso)
);
CREATE TABLE insegna(
    id_docente BIGINT,
    id_corso BIGINT,
    PRIMARY KEY(id_docente, id_corso),
    FOREIGN KEY(id_docente) REFERENCES docente(userId),
    FOREIGN KEY(id_corso) REFERENCES corso(id_corso)
);
CREATE TABLE segue(
    id_studente BIGINT,
    id_corso BIGINT,
    PRIMARY KEY(id_studente, id_corso),
    FOREIGN KEY(id_studente) REFERENCES studente(userId),
    FOREIGN KEY(id_corso) REFERENCES corso(id_corso)
);